import axios from "axios";

export const getToken = (username) =>
  axios
    .get(
      `https://945e-2409-4042-4cb5-c852-d1c8-bdd0-c0a0-29c9.ngrok.io/token/${username}`
    )
    .then((twilioUser) => twilioUser.data.jwt);
