import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";

import axios from "axios";
import { colors } from "../../theme";
import { routes } from "../../app";

export function MessageList({ navigation, route }) {
  const [listsms, setListSms] = useState([]);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={styles.addButton}
          onPress={() => navigation.navigate(routes.Messaging.name)}
        >
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
      ),
    });
  }, [navigation]);
  useEffect(() => {
    axios
      .get(
        "https://945e-2409-4042-4cb5-c852-d1c8-bdd0-c0a0-29c9.ngrok.io/smsListing"
      )
      .then((list) => {
        setListSms(list.data);
      });
  }, [listsms]);
  return (
    <SafeAreaView style={styles.screen}>
      <FlatList
        data={listsms}
        keyExtractor={(item) => item.sid}
        renderItem={({ item }) => (
          <TouchableOpacity style={styles.card}>
            {/* <Image style={styles.cardIcon} source={images.message} /> */}
            {item.imageUrls &&
              item.imageUrls.map((url, i) => {
                {
                  console.log(url, "url");
                }
                return (
                  <Image
                    style={styles.cardIcon}
                    key={i}
                    source={{ uri: url }}
                  />
                );
              })}
            <Text style={styles.cardText}>{item.body}</Text>
          </TouchableOpacity>
        )}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "snow",
  },
  addButton: {
    height: 24,
    width: 24,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 8,
  },
  addButtonText: {
    fontSize: 22,
    fontWeight: "700",
    lineHeight: 24,
    color: "white",
  },
  card: {
    flexDirection: "row",
    alignItems: "center",
    shadowColor: colors.windsor,
    backgroundColor: colors.white,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,
    elevation: 1,
    borderRadius: 10,
    marginHorizontal: 12,
    marginTop: 12,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  cardIcon: {
    height: 300,
    width: 300,
  },
  cardText: {
    fontSize: 16,
    marginLeft: 24,
    marginRight: 8,
    color: colors.cinder,
  },
});
