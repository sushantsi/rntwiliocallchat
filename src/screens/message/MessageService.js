import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useState } from "react";

import axios from "axios";
import { colors } from "../../theme";
import { images } from "../../assets";
import { routes } from "../../app";

export function MessageService({ navigation }) {
  const [message, setmessage] = useState("");

  return (
    <View style={styles.screen}>
      <Image style={styles.logo} source={images.logo} />
      <Text style={styles.titleText}>Welcome to Twilio messaging</Text>
      <TextInput
        value={message}
        onChangeText={setmessage}
        style={styles.input}
        placeholder="Text to be send"
        placeholderTextColor={colors.ghost}
      />
      <TouchableOpacity
        disabled={!message}
        style={styles.button}
        onPress={async () => {
          try {
            const data = await axios.post(
              "https://945e-2409-4042-4cb5-c852-d1c8-bdd0-c0a0-29c9.ngrok.io/sms",
              { message },
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
          } catch (error) {
            console.log(error);
          }
        }}
      >
        <Text
          style={styles.buttonText}
          // onPress={() => navigation.navigate(routes.MessageList.name)}
        >
          submit
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.snow,
  },
  logo: {
    width: 120,
    height: 120,
    marginBottom: 32,
  },
  titleText: {
    fontSize: 20,
    fontWeight: "700",
    color: colors.amaranth,
  },
  input: {
    width: 280,
    height: 150,
    padding: 12,
    fontSize: 16,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: colors.eclipse,
    marginTop: 32,
    marginBottom: 16,
  },
  button: {
    width: 280,
    height: 50,
    backgroundColor: colors.malibu,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: 17,
    color: colors.white,
  },
});
