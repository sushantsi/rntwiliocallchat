import {
  ActivityIndicator,
  Button,
  Platform,
  StyleSheet,
  TextInput,
  View,
} from "react-native";
import {
  EventType,
  RNTwilioPhone,
  twilioPhoneEmitter,
} from "react-native-twilio-phone";
import React, { useEffect, useState } from "react";

import RNCallKeep from "react-native-callkeep";
import Twilio from "react-native-twilio";
import { routes } from "../../app";

const identity = Platform.select({
  ios: "Steve",
  android: "alice",
});

const from = Platform.select({
  ios: "client:Steve",
  android: "client:alice",
  // android: +919021842317,
});

const callKeepOptions = {
  ios: {
    appName: "TwilioPhone Example",
    supportsVideo: false,
    maximumCallGroups: 10,
    maximumCallsPerCallGroup: 10,
  },
  android: {
    alertTitle: "Permissions required",
    alertDescription: "This application needs to access your phone accounts",
    cancelButton: "Cancel",
    okButton: "OK",
    additionalPermissions: [],
    // Required to get audio in background when using Android 11
    foregroundService: {
      channelId: "com.example.reactnativetwiliophone",
      channelName: "Foreground service for my app",
      notificationTitle: "My app is running on background",
    },
  },
};

export function Call({ navigation }) {
  const [token, setToken] = useState();
  async function fetchAccessToken(tokenSet) {
    // const response = await fetch('https://angry-duck-92.loca.lt/accessToken');
    const response = await fetch(
      // "https://fa29-206-183-111-25.ngrok.io/token/generate"
      "https://08dd-2401-4900-55c7-d114-2ca3-b17d-503e-496c.ngrok.io/token/generate",
      // "https://dabadu-sip-9073-dev.twil.io/admin/login",
      // "https://black-husky-88.loca.lt/accessToken"
      {
        method: "POST",
        body: JSON.stringify({
          identity: "Test",
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }
    );
    // const accessToken = await response.text();
    const accessToken = await response.json();
    // RNCallKeep.displayIncomingCall(
    //   accessToken.token,
    //   "5558909659",
    //   "Some User",
    //   undefined,
    //   true
    // );
    console.log("accessToken", accessToken);
    setToken(accessToken.token);
    // setToken(
    //   // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzZmM2RkOGE5MzZjYzk3ODVlNjk5NzdlZjE1NjEzOTg5LTE2NDIwNTc3MzIiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJ2aW5heSIsInZvaWNlIjp7ImluY29taW5nIjp7ImFsbG93Ijp0cnVlfSwib3V0Z29pbmciOnsiYXBwbGljYXRpb25fc2lkIjoiQVBhNTk2OGJiZDA1OGVlMmMyY2RkM2U5MzEwZGJmNDE0ZiJ9fX0sImlhdCI6MTY0MjA1NzczMiwiZXhwIjoxNjQyMDYxMzMyLCJpc3MiOiJTSzZmM2RkOGE5MzZjYzk3ODVlNjk5NzdlZjE1NjEzOTg5Iiwic3ViIjoiQUNjMTY0MzJmN2ZjYjllOTdmMGFmNmYxNTAxNmFlYjgyMCJ9.-UDs0ve64Pzd1J6VSUqRBQeoJvJKJtlY61zPJIAN85E"
    // );
    Twilio.initWithToken(
      // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzZmM2RkOGE5MzZjYzk3ODVlNjk5NzdlZjE1NjEzOTg5LTE2NDIwNTc3MzIiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJ2aW5heSIsInZvaWNlIjp7ImluY29taW5nIjp7ImFsbG93Ijp0cnVlfSwib3V0Z29pbmciOnsiYXBwbGljYXRpb25fc2lkIjoiQVBhNTk2OGJiZDA1OGVlMmMyY2RkM2U5MzEwZGJmNDE0ZiJ9fX0sImlhdCI6MTY0MjA1NzczMiwiZXhwIjoxNjQyMDYxMzMyLCJpc3MiOiJTSzZmM2RkOGE5MzZjYzk3ODVlNjk5NzdlZjE1NjEzOTg5Iiwic3ViIjoiQUNjMTY0MzJmN2ZjYjllOTdmMGFmNmYxNTAxNmFlYjgyMCJ9.-UDs0ve64Pzd1J6VSUqRBQeoJvJKJtlY61zPJIAN85E"
      accessToken.token
    );
    return accessToken.token;
    // return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzZmM2RkOGE5MzZjYzk3ODVlNjk5NzdlZjE1NjEzOTg5LTE2NDIwNTc3MzIiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJ2aW5heSIsInZvaWNlIjp7ImluY29taW5nIjp7ImFsbG93Ijp0cnVlfSwib3V0Z29pbmciOnsiYXBwbGljYXRpb25fc2lkIjoiQVBhNTk2OGJiZDA1OGVlMmMyY2RkM2U5MzEwZGJmNDE0ZiJ9fX0sImlhdCI6MTY0MjA1NzczMiwiZXhwIjoxNjQyMDYxMzMyLCJpc3MiOiJTSzZmM2RkOGE5MzZjYzk3ODVlNjk5NzdlZjE1NjEzOTg5Iiwic3ViIjoiQUNjMTY0MzJmN2ZjYjllOTdmMGFmNmYxNTAxNmFlYjgyMCJ9.-UDs0ve64Pzd1J6VSUqRBQeoJvJKJtlY61zPJIAN85E";
  }
  const [to, setTo] = React.useState("");
  const [callInProgress, setCallInProgress] = React.useState(false);

  // setToken(fetchAccessToken);
  React.useEffect(() => {
    return RNTwilioPhone.initialize(callKeepOptions, fetchAccessToken);
  }, []);

  React.useEffect(() => {
    const subscriptions = [
      twilioPhoneEmitter.addListener(EventType.CallConnected, () => {
        setCallInProgress(true);
      }),
      twilioPhoneEmitter.addListener(EventType.CallDisconnected, () => {
        setCallInProgress(RNTwilioPhone.calls.length > 0);
      }),
      twilioPhoneEmitter.addListener(
        EventType.CallDisconnectedError,
        (data) => {
          console.log(data);
          setCallInProgress(RNTwilioPhone.calls.length > 0);
        }
      ),
    ];

    return () => {
      subscriptions.map((subscription) => {
        subscription.remove();
      });
    };
  }, []);

  useEffect(() => {
    RNCallKeep.addEventListener("answerCall", () => {
      console.log("answerCall");
      console.log(token, "token");
      // RNCallKeep.startCall(token, +919021842317, "Test user");
      RNCallKeep.answerIncomingCall(token);
    });
    RNCallKeep.addEventListener("didPerformDTMFAction", () => {
      console.log("didPerformDTMFAction");
    });
    RNCallKeep.addEventListener("didReceiveStartCallAction", () => {
      console.log("didReceiveStartCallAction");
    });
    RNCallKeep.addEventListener("didPerformSetMutedCallAction", () => {
      console.log("didPerformSetMutedCallAction");
    });
    RNCallKeep.addEventListener("didToggleHoldCallAction", () => {
      console.log("didToggleHoldCallAction");
    });
    RNCallKeep.addEventListener("endCall", () => {
      console.log("endCall");
      RNCallKeep.endCall(token);
    });

    return () => {
      RNCallKeep.removeEventListener("answerCall", () => {
        console.log("answerCall");
      });
      RNCallKeep.removeEventListener("didPerformDTMFAction", () => {
        console.log("didPerformDTMFAction");
      });
      RNCallKeep.removeEventListener("didReceiveStartCallAction", () => {
        console.log("didReceiveStartCallAction");
      });
      RNCallKeep.removeEventListener("didPerformSetMutedCallAction", () => {
        console.log("didPerformSetMutedCallAction");
      });
      RNCallKeep.removeEventListener("didToggleHoldCallAction", () => {
        console.log("didToggleHoldCallAction");
      });
      RNCallKeep.removeEventListener("endCall", () => {
        console.log("endCall");
      });
    };
  }, []);
  useEffect(() => {
    Twilio.addEventListener("deviceReady", () =>
      console.log("device is ready")
    );
    Twilio.addEventListener("deviceDidStartListening", () =>
      console.log("device has started listening")
    );
    Twilio.addEventListener("deviceDidStopListening", () => {
      console.log("device has stoped listening");
    });
    Twilio.addEventListener("deviceDidReceiveIncoming", () => {
      console.log("deviceDidReceiveIncoming");
    });
    Twilio.addEventListener("connectionDidStartConnecting", () => {
      console.log("connectionDidStartConnecting");
    });
    Twilio.addEventListener(
      "connectionDidConnect",
      console.log("connectionDidConnect")
    );

    Twilio.addEventListener("connectionDidDisconnect", () => {
      console.log("connectionDidDisconnect");
    });
    Twilio.addEventListener("connectionDidFail", () =>
      console.log("connectionDidFail")
    );
    // setTimeout(() => {
    //   Twilio.connect({ To: "918878606936" });
    // }, 6000);
    // return () => {
    //   cleanup;
    // };
  }, []);

  function hangup() {
    RNCallKeep.endAllCalls();
  }

  async function call() {
    Twilio.connect({ To: to });
    console.log("to", to);
    console.log("from", from);
    if (to === "") {
      return;
    }

    setCallInProgress(true);
    // try {
    //   let dat = await RNTwilioPhone.startCall(to, "My friend", from, {
    //     calltype: "pstn",
    //   });
    //   //   let dat = await RNTwilioPhone.startCall(to);
    //   console.log("dat", dat);
    // } catch (e) {
    //   console.log(e);
    //   setCallInProgress(false);
    // }
  }
  async function conferance() {
    console.log("to", to);
    console.log("from", from);
    if (to === "") {
      return;
    }

    setCallInProgress(true);

    try {
      // let dat = await RNTwilioPhone.startCall(to, 'My friend', from);
      let dat = await RNTwilioPhone.startCall(
        to,
        "My friend",
        +919021842317,
        // +12292104806,
        { calltype: "conference" }
      );
      console.log("dat", dat);
    } catch (e) {
      console.log(e);
      setCallInProgress(false);
    }
  }
  async function sip() {
    console.log("to", to);
    console.log("from", from);
    if (to === "") {
      return;
    }

    setCallInProgress(true);

    try {
      // let dat = await RNTwilioPhone.startCall(to, 'My friend', from);
      let dat = await RNTwilioPhone.startCall(
        "sip:alice@dabadu-sip-9073-dev.sip.us1.twilio.com;transport=UDP",
        "My friend",
        "alice",
        {
          calltype: "sip",
        }
      );

      console.log("dat", dat);
    } catch (e) {
      console.log(e);
      setCallInProgress(false);
    }
  }

  async function unregister() {
    try {
      await RNTwilioPhone.unregister();
    } catch (e) {
      console.log(e);
    }
  }

  let content;

  if (callInProgress) {
    content = (
      <View>
        <ActivityIndicator color="#999" style={styles.loader} />
        <Button title="End call" onPress={hangup} />
      </View>
    );
  } else {
    content = (
      <View>
        <TextInput
          style={styles.to}
          onChangeText={(text) => setTo(text)}
          value={to}
          placeholder="Client or phone number"
          placeholderTextColor="gray"
        />
        <Button title="Start call" onPress={call} />
        <Button title="Start conferance" onPress={conferance} />
        <Button title="Start sip call" onPress={sip} />
        <View style={styles.unregister}>
          <Button title="Unregister" onPress={unregister} />
        </View>
      </View>
    );
  }

  return (
    <>
      <View style={{ padding: 50 }}>
        <View style={styles.unregister}>
          <Button
            title="Chat"
            onPress={() => navigation.navigate(routes.Welcome.name)}
          />
        </View>
        <View style={styles.unregister}>
          <Button
            title="Messsage"
            onPress={() => navigation.navigate(routes.MessageList.name)}
          />
        </View>
      </View>
      <View style={styles.container}>{content}</View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  loader: {
    marginBottom: 40,
  },
  to: {
    height: 50,
    width: 200,
    fontSize: 16,
    borderColor: "gray",
    borderBottomWidth: 1,
    marginBottom: 40,
    color: "gray",
    textAlign: "center",
  },
  unregister: {
    marginTop: 40,
  },
});
